# Team View Component

# Requirements

- React
- TypeScript
- npm

### Plus

- Redux
- Jest / React Testing Library 

# Running it locally

- `npm install`
- `npm start`
- Open http://localhost:3000 to view it in the browser.

# Running tests

- `npm run test`

# Additonals

The team view was made by basic four components `AddMember`, `Dropdown`, `ShowAllMembers` and `TeamMember`.
I choose to broke into four components following the "Thinking in React" practice. In each component is your style and test file, minus the App.
Why I did this way? Well, I think that's the best way to structure a project and makes it easier to read code from each part of the component

Of course, in this project the domain was very simple, but I tried to demonstrate my skills here. Using sass, unit tests and not using any library for component or style.
