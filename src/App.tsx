import React, { ChangeEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { allSelectUsers, selectUsers } from './redux/store';
import { addUser, removeUser } from './redux/users';
import DropDown from './components/DropDown/DropDown';
import AddMember from './components/AddMember/AddMember';
import TeamMember from './components/TeamMember/TeamMember';
import ShowAllMembers from './components/ShowAllMembers/ShowAllMembers';
import useFocusOut from './hooks/useFocusOut';

// styles
import './styles/Global.scss';
import './styles/App.scss';

// images
import groupImage from './assets/imgs/group.svg';
import { UserInterface } from './assets/data';

function App() {
  const [userName, setUserName] = useState('');
  const [selectedUserList, setSelectedUserList] = useState<UserInterface[]>([]);
  const [newUserActive, setNewUserActive] = useState(false);
  const [showDropdown, setShowDropdown] = useState(false);
  const [needShowAll, setNeedShowAll] = useState(false);
  const users = useSelector(selectUsers);
  const allUsers = useSelector(allSelectUsers);
  const dispatch = useDispatch();

  const useRefHook = useFocusOut(() => {
    setNewUserActive(false);
    setShowDropdown(false);
  });

  const onAddUser = (user: UserInterface) => {
    dispatch(addUser(user));
    setShowDropdown(false);
    setNewUserActive(false);
    setUserName('');
  };

  const enterNewUser = () => {
    setNewUserActive(true);
    setShowDropdown(true);
  };

  const changeUserName = (e: ChangeEvent<HTMLInputElement>): void => {
    setUserName(e?.target?.value);
  };

  const onRemoveUser = (user: UserInterface) => {
    dispatch(removeUser(user));
  };

  const toggleDisplayUsers = () => {
    setNeedShowAll(!needShowAll);
  };

  const clearInput = () => {
    setUserName('');
  };

  useEffect(() => {
    const selectedUsers = allUsers.filter((user: UserInterface) =>
      user.username.toLocaleLowerCase().includes(userName.toLowerCase()),
    );
    setSelectedUserList(selectedUsers);
  }, [allUsers, userName]);

  return (
    <>
      <h1 className="title">Team View Component - Testbirds</h1>
      <div className="main-container">
        <div className="team-title-container">
          <h3 className="subtitles">Your team for this test</h3>
          <div className="team-page-icon">
            <span>Team page</span>
            <img src={groupImage} alt="Group Icon" />
          </div>
        </div>
        <ul className="members-list-container" ref={useRefHook}>
          <AddMember
            userName={userName}
            changeUserName={changeUserName}
            newUserActive={newUserActive}
            enterNewUser={enterNewUser}
            clearInput={clearInput}
          />
          <DropDown
            showDropdown={showDropdown}
            onAddUser={onAddUser}
            listOfSelectedUsers={selectedUserList}
          />
          {users
            .map((user: UserInterface) => (
              <TeamMember
                key={user.id}
                user={user}
                onRemoveUser={onRemoveUser}
              />
            ))
            .splice(0, needShowAll ? users.length : 5)}
        </ul>
        <ShowAllMembers
          needShowAll={needShowAll}
          toggleDisplayUsers={toggleDisplayUsers}
          renderButton={users.length > 5}
        />
      </div>
    </>
  );
}

export default App;
