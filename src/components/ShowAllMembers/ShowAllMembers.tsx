//style
import "./ShowAllMembers.scss";

export interface ShowAllMembersInterface {
  renderButton: boolean;
  needShowAll: boolean;
  toggleDisplayUsers: () => void;
}

const ShowAllMembers = ({
  renderButton,
  needShowAll,
  toggleDisplayUsers,
}: ShowAllMembersInterface) => {
  const buttonText = needShowAll ? "Show less" : "Show all";
  if (renderButton) {
    return (
      <div className="buttons">
        <button className="show-all-btn" onClick={toggleDisplayUsers}>
          {buttonText}
        </button>
      </div>
    );
  }
  return null;
};

export default ShowAllMembers;
