import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import ShowAllMembers from './ShowAllMembers';

describe('ShowAllMembers', () => {
  const callback = jest.fn();

  it('Should render ShowAllMembers Component when renderButton is true', () => {
    const { container } = render(
      <ShowAllMembers needShowAll toggleDisplayUsers={callback} renderButton />,
    );
    expect(container).toBeInTheDocument();
  });

  it("Shouldn't render ShowAllMembers Component when renderButton is false", () => {
    const { container } = render(
      <ShowAllMembers
        needShowAll
        toggleDisplayUsers={callback}
        renderButton={false}
      />,
    );
    expect(container).not.toHaveClass('buttons');
  });

  it('Should render Show all when needShowAll is false', () => {
    render(
      <ShowAllMembers
        needShowAll={false}
        toggleDisplayUsers={callback}
        renderButton
      />,
    );
    expect(screen.getByText('Show all')).toBeInTheDocument();
  });

  it('Should render Show less when needShowAll is true', () => {
    render(
      <ShowAllMembers
        needShowAll={true}
        renderButton
        toggleDisplayUsers={callback}
      />,
    );
    expect(screen.getByText('Show less')).toBeInTheDocument();
  });

  //   it("Should render optianal title", () => {
  //     render(<BrandAlert alertText="Test" alertTitle="Optional title" />);
  //     expect(screen.getByText("Optional title")).toBeInTheDocument();
  //   });

  //   it("Should render optional close button", () => {
  //     render(<BrandAlert alertText="Test" hasCloseButton />);
  //     expect(screen.getByTestId("brandAlert--closeBtn")).toBeInTheDocument();
  //   });
  //   it("Should not render optional close button", () => {
  //     render(<BrandAlert alertText="Test" />);
  //     expect(
  //       screen.queryByTestId("brandAlert--closeBtn")
  //     ).not.toBeInTheDocument();
  //   });

  //   it("Should hide the component when clicks on close button", async () => {
  //     render(<BrandAlert alertText="Test" hasCloseButton />);
  //     const closeButton = screen.getByTestId("brandAlert--closeBtn");
  //     fireEvent.click(closeButton);
  //     await waitFor(() => {
  //       expect(screen.queryByText("Test")).not.toBeInTheDocument();
  //     });
  //   });

  //   it("Should render warning styles when type is warning", () => {
  //     render(<BrandAlert alertText="Test" type={AlertType.WARNING} />);
  //     expect(screen.queryByTestId("WarningRoundedIcon")).toBeInTheDocument();
  //     expect(screen.queryByTestId("brandAlert")).toHaveClass(
  //       "brandAlert--warningColor"
  //     );
  //   });

  //   it("Should render success styles when type is success", () => {
  //     render(<BrandAlert alertText="Test" type={AlertType.SUCCESS} />);
  //     expect(screen.queryByTestId("CheckCircleRoundedIcon")).toBeInTheDocument();
  //     expect(screen.queryByTestId("brandAlert")).toHaveClass(
  //       "brandAlert--successColor"
  //     );
  //   });

  //   it("Should render success styles when type is danger", () => {
  //     render(<BrandAlert alertText="Test" type={AlertType.DANGER} />);
  //     expect(screen.queryByTestId("ErrorRoundedIcon")).toBeInTheDocument();
  //     expect(screen.queryByTestId("brandAlert")).toHaveClass(
  //       "brandAlert--dangerColor"
  //     );
  //   });

  //   it("Should render success styles when type is info", () => {
  //     render(<BrandAlert alertText="Test" type={AlertType.INFO} />);
  //     expect(screen.queryByTestId("InfoIcon")).toBeInTheDocument();
  //     expect(screen.queryByTestId("brandAlert")).toHaveClass(
  //       "brandAlert--infoColor"
  //     );
  //   });
});
