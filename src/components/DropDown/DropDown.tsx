// imagens
import { UserInterface } from '../../assets/data';
import person from '../../assets/imgs/avatar-default.png';

// styles
import './DropDown.scss';
interface DropdownMenuInterface {
  showDropdown: boolean;
  listOfSelectedUsers: UserInterface[];
  onAddUser: (user: UserInterface) => void;
}
const DropdownMenu = ({
  showDropdown,
  listOfSelectedUsers,
  onAddUser,
}: DropdownMenuInterface) => {
  if (showDropdown) {
    return (
      <div className="dropdown-container">
        {listOfSelectedUsers.map((user) => {
          return (
            <li
              className="member"
              key={user.id}
              onClick={() => onAddUser(user)}
            >
              <img src={person} alt="Person Icon" />
              <span className="member-name">{user.username}</span>
            </li>
          );
        })}
        {!listOfSelectedUsers.length && (
          <div className="not-found">
            <p>Team member not found.</p>
            <span>Maybe she/he is not in your team?</span>
          </div>
        )}
      </div>
    );
  }
  return null;
};

export default DropdownMenu;
