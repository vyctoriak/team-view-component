import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import './DropDown';
import DropdownMenu from './DropDown';

const userMock = [
  {
    username: 'teste',
    role: 'admin',
    picture: 'teste',
    id: 1,
  },
];

const mockFn = jest.fn;

describe('DropdownMenu', () => {
  it('Should render DropdownMenu Component when showDropdown is true', () => {
    const { container } = render(
      <DropdownMenu
        showDropdown={true}
        listOfSelectedUsers={userMock}
        onAddUser={mockFn}
      />,
    );
    expect(container).toBeInTheDocument();
  });

  it('Should not render members when showDropdown is false', () => {
    const { container } = render(
      <DropdownMenu
        showDropdown={false}
        listOfSelectedUsers={userMock}
        onAddUser={mockFn}
      />,
    );
    expect(container).not.toHaveClass('member-item-container');
  });

  it('Should member', () => {
    render(
      <DropdownMenu
        showDropdown={true}
        listOfSelectedUsers={userMock}
        onAddUser={mockFn}
      />,
    );
    const memberInside = screen.getByText('teste');
    expect(memberInside).toBeInTheDocument();
  });
});
