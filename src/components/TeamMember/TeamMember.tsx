import { UserInterface } from '../../assets/data';
import person from '../../assets/imgs/avatar-default.png';
import close from '../../assets/imgs/close.svg';

// style
import './TeamMember.scss';

interface TeamMemberInterface {
  user: UserInterface;
  onRemoveUser: (event: UserInterface) => void;
}

const TeamMember = ({ user, onRemoveUser }: TeamMemberInterface) => {
  const userRoleClass =
    user.role.toLowerCase() === 'external' ? 'user-external' : 'user-role';
  return (
    <li
      className="member-item-container"
      data-testid="member-imgs-container"
      onClick={() => onRemoveUser(user)}
    >
      <div className="imgs-container">
        <img className="member-photo" src={person} alt="Member" />
        <img className="close-icon" src={close} alt="Close Icon" />
        <div className="tooltip">Remove user</div>
      </div>
      <div className="member-infos">
        <span className={userRoleClass}>{user.role}</span>
        <span className="username">{user.username}</span>
      </div>
    </li>
  );
};

export default TeamMember;
