import '@testing-library/jest-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import TeamMember from './TeamMember';

const mockFn = jest.fn;

describe('Team Member', () => {
  it('Should render TeamMember component', () => {
    const userMock = {
      username: 'teste',
      role: 'teste',
      picture: 'teste',
      id: 1,
    };

    const { container } = render(
      <TeamMember user={userMock} onRemoveUser={mockFn} />,
    );

    expect(container).toBeInTheDocument();
  });

  it('Should render a external member if role external is true', () => {
    const userMock = {
      username: 'teste',
      role: 'external',
      picture: 'teste',
      id: 1,
    };
    render(<TeamMember user={userMock} onRemoveUser={mockFn} />);
    const external = screen.getByText('external');
    expect(external).toHaveClass('user-external');
  });

  it('Should render not render a external member if role is not external', () => {
    const userMock = {
      username: 'teste',
      role: 'admin',
      picture: 'teste',
      id: 1,
    };
    render(<TeamMember user={userMock} onRemoveUser={mockFn} />);
    const admin = screen.getByText('admin');
    expect(admin).not.toHaveClass('user-external');
  });

  it('Should remove a member when close icon is clicked', () => {
    const userMock = {
      username: 'teste',
      role: 'admin',
      picture: 'teste',
      id: 1,
    };
    render(<TeamMember user={userMock} onRemoveUser={mockFn} />);
    fireEvent.click(screen.getByAltText('Close Icon'));
    expect(screen.getByTestId('member-imgs-container')).not.toHaveClass(
      'imgs-container',
    );
  });
});
