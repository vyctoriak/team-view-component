import '@testing-library/jest-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import AddMember from './AddMember';

describe('AddMember', () => {
  it('Should render AddMember Component when clicked', () => {
    const mockClick = jest.fn();
    render(
      <AddMember
        clearInput={mockClick}
        userName=""
        changeUserName={mockClick}
        newUserActive={false}
        enterNewUser={mockClick}
      />,
    );
    fireEvent.click(screen.getByText('Add team member to this test'));
    expect(mockClick).toHaveBeenCalled();
  });

  it('Should open input when newUserActive is true', () => {
    const mockFn = jest.fn();
    render(
      <AddMember
        clearInput={mockFn}
        userName=""
        changeUserName={mockFn}
        newUserActive={true}
        enterNewUser={mockFn}
      />,
    );
    const button = screen.getByPlaceholderText('Client');
    expect(button).toBeInTheDocument();
  });

  it('Should not open input element when newUserActive is false', () => {
    const mockFn = jest.fn();
    render(
      <AddMember
        clearInput={mockFn}
        userName=""
        changeUserName={mockFn}
        newUserActive={false}
        enterNewUser={mockFn}
      />,
    );
    const button = screen.getByText('Add team member to this test');
    expect(button).toBeInTheDocument();
  });
});
