// images
import { ChangeEvent } from 'react';
import iconPlus from '../../assets/imgs/plus.svg';
import iconClose from '../../assets/imgs/close.svg';

// style
import './AddMember.scss';

interface AddMemberInterface {
  userName: string;
  changeUserName: (e: ChangeEvent<HTMLInputElement>) => void;
  newUserActive: boolean;
  enterNewUser: () => void;
  clearInput: () => void;
}
const AddMember = ({
  userName,
  changeUserName,
  newUserActive,
  enterNewUser,
  clearInput,
}: AddMemberInterface): JSX.Element => {
  return (
    <>
      {newUserActive ? (
        <div className="input-container">
          <input
            data-testid="insert-user"
            className="insert-user"
            type="text"
            value={userName}
            onChange={(e: ChangeEvent<HTMLInputElement>) => changeUserName(e)}
            placeholder="Client"
            autoFocus
          />
          <img
            src={iconClose}
            alt="Close Icon"
            className="close-icon"
            onClick={clearInput}
          />
        </div>
      ) : (
        <div className="team-list-container" data-testid="team-list-container">
          <div className="add-member-container" onClick={enterNewUser}>
            <div className="icon-plus">
              <img src={iconPlus} alt="Open/Plus Icon" />
            </div>
            <div className="add-title">
              <span>Add team member to this test</span>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default AddMember;
