import { useRef, useEffect } from 'react';

const useFocusOut = (handler: () => void): any => {
  const useRefHook = useRef<any>(null);

  useEffect(() => {
    const mouseEvent = (e: any) => {
      if (useRefHook.current && !useRefHook.current.contains(e.target)) {
        handler();
      }
    };

    document.addEventListener('mousedown', mouseEvent);

    return () => {
      document.removeEventListener('mousedown', mouseEvent);
    };
  }, [handler]);

  return useRefHook;
};

export default useFocusOut;
