import { configureStore } from '@reduxjs/toolkit';
import userReducer from './users';

const store = configureStore({
  reducer: {
    users: userReducer,
  },
});

export const selectUsers = (state) => state.users.users;
export const allSelectUsers = (state) => state.users.allUsers;
export default store;
